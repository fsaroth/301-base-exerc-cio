FROM openjdk:8-jdk-alpine AS JDK
COPY ./ ./
FROM openjdk:8-jre-alpine
COPY --from=JDK /target/Api-Investimentos-*.jar /api-investimentos.jar
CMD ["java", "-jar", "api-investimentos.jar"]
